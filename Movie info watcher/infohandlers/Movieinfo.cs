﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie_info_watcher
{
    public class Movieinfo
    {
        public string Title { get; set; }
        public float Rottentomatoerating { get; set; }
        public string Description { get; set; }
        public DateTime Releasedate { get; set; }
        public string Director { get; set; }
        public string Boxoffice { get; set; }
        public float Rating { get; set; }
        public string Picurl { get; set; }


    }
}
