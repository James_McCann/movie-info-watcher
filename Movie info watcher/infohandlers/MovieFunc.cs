﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using HtmlAgilityPack;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace Movie_info_watcher.infohandlers
{
    class MovieFunc
    {
        public static Movieinfo GetinfoMovieinfo(string filename)
        {
            Movieinfo movieinfo = new Movieinfo();
            WebClient client = new WebClient();
            HtmlDocument doc = new HtmlDocument();
            client.QueryString.Add(new NameValueCollection { { "q", filename } });
            doc.LoadHtml(client.DownloadString("http://www.google.com/search"));
            string xpath = "//div[contains(@class, 'xp')]";
            //xpath = "//*[@id=\"rcnt\"]";
            var nodes = doc.DocumentNode.SelectNodes(xpath)?[0].ChildNodes;
            if (nodes == null)
                return null;

            int i = 0;
            foreach (var r1 in nodes)
            {
                //MessageBox.Show($@"{r1.InnerText} // {r1.ChildNodes.Count} || {r1.InnerHtml}", $"{i}");

                if (r1.InnerHtml.Contains("_i8d"))
                {
                    movieinfo.Picurl = r1.InnerHtml.Split('"')[7];
                }
                if (r1.InnerHtml.Contains("_tXc"))
                {
                    movieinfo.Description = r1.InnerText;
                }
                i++;
            }



            return movieinfo;
        }
    }
}
