﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using Movie_info_watcher.infohandlers;

namespace Movie_info_watcher
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            listBox1.Items.Add("the new guy");
            listBox1.Items.Add("mortal engines movie");
            listBox1.Items.Add("pacific rim uprising");
            listBox1.Items.Add("jurassic world fallen kingdom");
            listBox1.Items.Add("oasis movie");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(textBox1.Text);
        }

        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listBox1.SelectedIndex == -1)
                return;
            
            //_movieHandler.Getinfo(listBox1.SelectedItem.ToString());

            

            moviePanel1.Update(MovieFunc.GetinfoMovieinfo(listBox1.SelectedItem.ToString()));
        }
    }
}
