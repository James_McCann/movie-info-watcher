﻿namespace Movie_info_watcher
{
    partial class MoviePanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picMovie = new System.Windows.Forms.PictureBox();
            this.labDescription = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picMovie)).BeginInit();
            this.SuspendLayout();
            // 
            // picMovie
            // 
            this.picMovie.Location = new System.Drawing.Point(504, 7);
            this.picMovie.Name = "picMovie";
            this.picMovie.Size = new System.Drawing.Size(107, 153);
            this.picMovie.TabIndex = 16;
            this.picMovie.TabStop = false;
            // 
            // labDescription
            // 
            this.labDescription.AutoSize = true;
            this.labDescription.Location = new System.Drawing.Point(8, 7);
            this.labDescription.MaximumSize = new System.Drawing.Size(500, 0);
            this.labDescription.Name = "labDescription";
            this.labDescription.Size = new System.Drawing.Size(35, 13);
            this.labDescription.TabIndex = 15;
            this.labDescription.Text = "label1";
            // 
            // MoviePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picMovie);
            this.Controls.Add(this.labDescription);
            this.Name = "MoviePanel";
            this.Size = new System.Drawing.Size(622, 170);
            ((System.ComponentModel.ISupportInitialize)(this.picMovie)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picMovie;
        private System.Windows.Forms.Label labDescription;
    }
}
