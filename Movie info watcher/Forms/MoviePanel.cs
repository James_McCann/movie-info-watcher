﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Movie_info_watcher
{
    public partial class MoviePanel : UserControl
    {
        public MoviePanel()
        {
            InitializeComponent();
        }

        public void Update(Movieinfo info)
        {
            labDescription.Text = info.Description;
            picMovie.Load(info.Picurl);
        }
    }
}
